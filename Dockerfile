FROM registry.access.redhat.com/ubi8/php-74:1

USER root 

ENV SITE_ROOT=/opt/app-root/src \
    REPO=https://gitlab.gnome.org/Infrastructure/foundation-web.git

COPY app_data ${SITE_ROOT}
RUN mkdir ${SITE_ROOT}/html ${SITE_ROOT}/src && \
    yum install git automake -y && \
    git clone ${REPO} ${SITE_ROOT}/src && \
    cd ${SITE_ROOT}/src && \
    ./autogen.sh --prefix=${SITE_ROOT}/html && make && make install && \
    chown -R 1001:0 /opt/app-root/src

EXPOSE 8080

USER 1001
CMD /usr/libexec/s2i/run
